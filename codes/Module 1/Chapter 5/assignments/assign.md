
![Virtual Labs](image.png "Click Here")
# Colloid and Surface Chemistry Lab
*This lab is about colloidal systems. Milk, butter, ice-cream, body-spray, ink, shaving cream, glue, etc. are a few of numerous examples of colloids. We know that there are three fundamental states of matter: solids, liquids and gases. When matter in one of these states is dispersed in finely divided state in another, it is called a ‘colloidal system’. Importance of colloidal systems cannot be exaggerated. 
*These colloidal systems have interesting properties that are often substantially different from their individual constituents. Colloidal systems find applications in day-to-day life to industry, agriculture, medicine, biological sciences and present-day nanotechnology.
*Though colloid science has been part of UG and PG curricula for decades, resurgence of interest in it occurred recently due to the emergence of “bottom-up” nanotechnology approach. In a colloidal system, the dispersed phase is the one, which is present in finely divided state and the other continuous medium is known as the dispersion medium or dispersant. Colloidal particles are not visible under ordinary microscopes and pass through the pores of ordinary filter paper.
*Colloidal solutions are microscopically heterogeneous but macroscopically homogeneous. Colloidal solutions bridge the gap between true solutions (where ions and molecules are dispersed phase) and (coarse) suspensions (which often settle down under gravity). In a colloidal system, the dispersed phase may consist of particles of a single macromolecule (e.g. protein, starch) or an aggregate of many atoms, ions or molecules (e.g. silver colloid, micelle). 
*Although the size of the dispersed phase particles in a colloid is larger than ordinary molecules, they are small enough to remain suspended. The size of dispersed phase particles ranges from one nanometer (10-9 m) to one micrometer (10-6 m) or so (Table 1). In certain circumstances, the particles in a colloidal dispersion may form loose aggregates, called flocs (via flocculation) or irreversible aggregates (via coagulation).

>Table 1. Size range of dispersed particles of solutions, colloids and mixtures

|MIXTURES|COLLOIDS|SOLUTIONS|
|----|----|----|
|Large particles|Tiny Particles|Ions and Molecules|
|>~1µm|0.001 - ~1 µm|< 0.001 µm|

*Colloids are classified in many ways based on their nature, dispersion medium, etc. (Table 2). For example, there are various kinds of colloidal systems that include sols (solids dispersed in liquid), aerosol (liquid in gas), emulsions (liquid in liquid), gel (liquid in solid), association colloids or micelles (assembly of lyophobic-lyophilic molecules dispersed in liquid), etc.

>Table 2. Types of Colloids and Examples

>>TYPES OF COLLOIDS

|DISPERSED PHASE	|DISPERSANT PHASE|NAME|EXAMPLE|
|----|----|----|----|
|Solid|Gas|Smoke-Aerosol|Smoke|
|Liquid|Gas|Fog-Aerosol|Fog|
|Solid|Liquid|Sol|Paint|
|Liquid|Liquid|Emulsion|Milk, Mayonnaise|
|Gas|Liquid|Foam|Beer Foam|
|Solid|Solid|Solid suspension|Amethyst|
|Liquid|Solid|Solid emulsion, Gel|Jelly, Opal|
|Gas|Solid|Solid Foam|Pumice stone|


