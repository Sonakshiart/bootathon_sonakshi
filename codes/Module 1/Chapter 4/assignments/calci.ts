/**Initialisation and extraction of variables */
var in1 : HTMLInputElement = <HTMLInputElement>document.getElementById("in1");
var in2 : HTMLInputElement = <HTMLInputElement>document.getElementById("in2");
var ans1: HTMLInputElement = <HTMLInputElement>document.getElementById("ans1");

var in3 : HTMLInputElement = <HTMLInputElement>document.getElementById("in3");
var in4 : HTMLInputElement = <HTMLInputElement>document.getElementById("in4");
var ans2: HTMLInputElement = <HTMLInputElement>document.getElementById("ans2");

var in5 : HTMLInputElement = <HTMLInputElement>document.getElementById("in5");
var in6 : HTMLInputElement = <HTMLInputElement>document.getElementById("in6");
var ans3: HTMLInputElement = <HTMLInputElement>document.getElementById("ans3");

var in7: HTMLInputElement = <HTMLInputElement>document.getElementById("in7");
var in8: HTMLInputElement = <HTMLInputElement>document.getElementById("in8");
var ans4: HTMLInputElement = <HTMLInputElement>document.getElementById("ans4");
//export {};
function add() /**This function adds the two values given by user and prints it */
{
    var c:number = parseFloat(in1.value) + parseFloat(in2.value);
    ans1.value = c.toString();
}

function sub() /**This function substract the two values given by user and prints it */
{
    var c:number= parseFloat(in3.value)-parseFloat(in4.value);
    ans2.value = c.toString();
}

function multi() /**This function multiply the two values given by user and prints it */
{
    var c:number= parseFloat(in5.value)*parseFloat(in6.value);
    ans3.value = c.toString();
}

function div() /**This function divides the two values given by user and prints it */
{
    var c:number= parseFloat(in7.value)/parseFloat(in8.value);
    ans4.value = c.toString();
}
