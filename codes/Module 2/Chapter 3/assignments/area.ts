//fuction to check whether the point provided by user lies inside or outside the triangle

function checkPoint(){
    //Extraction of values
    let t11:HTMLInputElement=<HTMLInputElement>document.getElementById("t11");
    let t12:HTMLInputElement=<HTMLInputElement>document.getElementById("t12");
    let t21:HTMLInputElement=<HTMLInputElement>document.getElementById("t21");
    let t22:HTMLInputElement=<HTMLInputElement>document.getElementById("t22");
    let t31:HTMLInputElement=<HTMLInputElement>document.getElementById("t31");
    let t32:HTMLInputElement=<HTMLInputElement>document.getElementById("t32");
    let t41:HTMLInputElement=<HTMLInputElement>document.getElementById("t41");
    let t42:HTMLInputElement=<HTMLInputElement>document.getElementById("t42");

    //Initialisation of variables
    var x1:number=parseFloat(t11.value);
    var y1:number=parseFloat(t12.value);
    var x2:number=parseFloat(t21.value);
    var y2:number=parseFloat(t22.value);
    var x3:number=parseFloat(t31.value);
    var y3:number=parseFloat(t32.value);
    var x4:number=parseFloat(t41.value);
    var y4:number=parseFloat(t42.value);


    //Finding the lenght of sides of the main triange and the triangle formed by the given point
    var a=Math.sqrt(Math.pow((x2-x1),2)+Math.pow((y2-y1),2));
    console.log(a);

    var b=Math.sqrt(Math.pow((x1-x3),2)+Math.pow((y1-y3),2));
    console.log(b);

    var c=Math.sqrt(Math.pow((x3-x2),2)+Math.pow((y3-y2),2));
    console.log(c);

    var d=Math.sqrt(Math.pow((x1-x4),2)+Math.pow((y1-y4),2));
    console.log(d);
    
    var e=Math.sqrt(Math.pow((x2-x4),2)+Math.pow((y2-y4),2));
    console.log(e);

    var f=Math.sqrt(Math.pow((x3-x4),2)+Math.pow((y3-y4),2));
    console.log(f);

    //calculating the semiperimeter and area of each triangle

    var s1=(a+b+c)/2; //Semiperimeter of the main triangle
    var area1:number=Math.sqrt(s1*(s1-a)*(s1-b)*(s1-c)); //Semiperimeter of the main triangle
    

    //Following is the semiperimeter and area of other triangles
    var s2=(a+d+e)/2;
    var area2:number=Math.sqrt(s2*(s2-a)*(s2-d)*(s2-e));
    

    var s3=(d+f+b)/2;
    var area3:number=Math.sqrt(s3*(s3-d)*(s3-f)*(s3-b));
  

    var s4=(e+f+c)/2;
    var area4:number=Math.sqrt(s4*(s4-e)*(s4-f)*(s4-c));

    //Checking if the area of triangles formed by joining the vertex of main triangle to the given point 
    //is equal to the area of the main triangle  
    
    if(area1==area2+area3+area4)
    {
        alert("Points lies inside the triangle");
    }
    else
    {
        alert("Points lies outside the triangle");
    }
}



   

